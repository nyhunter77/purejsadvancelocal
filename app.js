// ================ getting berries ==================
// the way I understood, set a timeout for the response to return means here

let data; 
setTimeout(() => {
  (function getBerries() {
    const request = new XMLHttpRequest()
  
    request.open('GET', 'https://pokeapi.co/api/v2/berry-flavor/1/', true)
  
    request.onload = function () {
      data = JSON.parse(this.response);
      console.log(data);
      for (let i = 0; i < 5; i++) {
        localStorage.setItem(`berry[${i}]`, data.berries[i].berry.name); // get the first 5 berries
      }
    }
  
    request.send()
  })()
}, 1000)

let passedData; 
if (typeof(data) !== 'undefined' && data !== '') {
  // since we replaced local storage data when the berries come in, we can retrieve the newest data from storage
  passedData = [localStorage.getItem('berry[0]'),localStorage.getItem('berry[1]'),localStorage.getItem('berry[2]'),localStorage.getItem('berry[3]'),localStorage.getItem('berry[4]')]
} else {
  // berries have not arrived, check if we have localstorage or not
  if (!localStorage.getItem('berry[0]')) {
    passedData = ['none'];
  } else {
    passedData = [localStorage.getItem('berry[0]'),localStorage.getItem('berry[1]'),localStorage.getItem('berry[2]'),localStorage.getItem('berry[3]'),localStorage.getItem('berry[4]')]
  }
}


// ================ loading ads ==================

function buttonClicked() {
  const googletag = window.googletag || {};
  googletag.cmd = googletag.cmd || [];

  // randomly setting targeting - sets the first via coinflip and then other ad unit gets opposite category of targeting with random value
  const keyValuePairs = [{'interests':['sports','music','movies']},{'sports':['racing','cricket','baseball']}];
  const coinFlip = Math.round(Math.random());
  const fineTarget = Math.floor((Math.random()*3));

  const key = Object.keys(keyValuePairs[coinFlip]).toString();
  const key2 = Object.keys(keyValuePairs[coinFlip === 1 ? 0 : 1]).toString();

  let value;
  let value2;
  if (coinFlip === 1) {
    value = keyValuePairs[coinFlip].sports[fineTarget];
    value2 = keyValuePairs[0].interests[fineTarget];
  } else {
    value = keyValuePairs[coinFlip].interests[fineTarget];
    value2 = keyValuePairs[1].sports[fineTarget];
  }
  
  googletag.cmd.push(() => {
    googletag.defineSlot('344101295/SI/www.silive.com/news/index.ssf', [728, 90], 'headerAd').addService(googletag.pubads())
      .setTargeting(key, value) // slot level 
      .setTargeting('size', '728x90');
    googletag.defineSlot('344101295/SI/www.silive.com/news/index.ssf', [300, 250], 'body1').addService(googletag.pubads())
      .setTargeting(key2, value2) // slot level
      .setTargeting('size', '300x250');
    googletag.pubads().setTargeting("topic","basketball"); // page level
    googletag.pubads().setTargeting("berry", passedData); // page level for berry
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
    googletag.display('headerAd');
    googletag.display('body1');
  });
}